#include <stdlib.h>
#include "stack.h"

static struct dmStackNode *dmNewStackNode(void *data)
{
    struct dmStackNode *node = (struct dmStackNode*) malloc(sizeof(struct dmStackNode));
    node->data = data;
    node->next = NULL;
    return node;
}

struct dmStack *dmNewStack(void)
{
    struct dmStack *stack = (struct dmStack*) malloc(sizeof(struct dmStack));
    stack->top = NULL;
    return stack;
}

void dmStackPush(struct dmStack *stack, void *data)
{
    if (stack->top == NULL) {
        stack->top = dmNewStackNode(data);
    } else {
        struct dmStackNode *new_top = dmNewStackNode(data);
        new_top->next = stack->top;
        stack->top = new_top;
    }
}

int dmStackPop(struct dmStack *stack, void **result)
{
    if (stack->top == NULL)
        return -1;

    struct dmStackNode *node = stack->top;
    *result = node->data;
    stack->top = node->next;
    free(node);

    return 0;
}

void dmFreeStack(struct dmStack *stack)
{
    struct dmStackNode *curr = stack->top;
    while (curr != NULL) {
        struct dmStackNode *node = curr;
        curr = curr->next;
        free(node);
    }
    free(stack);
}
