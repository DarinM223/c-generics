#include <stdio.h>
#include <stdlib.h>
#include "stack.h"
#include "vector.h"

#define MAKEMIN(TYPE)               \
TYPE min_##TYPE (TYPE a, TYPE b) {  \
    if (a < b) {                    \
        return a;                   \
    } else {                        \
        return b;                   \
    }                               \
}

#define DOUBLE(a, res) do {     \
    if ((a) == 2) {             \
        (res) = 69;             \
    } else {                    \
        (res) = 4;              \
    }                           \
} while (0);

MAKEMIN(int);
MAKEMIN(float);
MAKEMIN(double);

void doStack(void)
{
    struct dmStack *stack = dmNewStack();

    int a = 2, b = 3;

    dmStackPush(stack, (void*)(&a));
    dmStackPush(stack, (void*)(&b));

    int *result;

    if (dmStackPop(stack, (void**)&result) == -1)
        goto errStack;

    printf("Popped value 1 is %d\n", *result);

    if (dmStackPop(stack, (void**)&result) == -1)
        goto errStack;

    printf("Popped value 2 is %d\n", *result);

    if (dmStackPop(stack, (void**)&result) == -1)
        goto errStack;

    printf("This shouldn't print\n");

errStack:
    dmFreeStack(stack);
}

void doVector(void)
{
    struct dmVector_int *v = dmNewVector_int();

    dmVectorPush_int(v, 2);
    dmVectorPush_int(v, 3);

    int idx0, idx1;

    if (dmVectorGet_int(v, 0, &idx0) == -1)
        goto errVec;

    printf("Value at index 0: %d\n", idx0);

    if (dmVectorGet_int(v, 1, &idx1) == -1)
        goto errVec;

    printf("Value at index 1: %d\n", idx1);

    if (dmVectorSet_int(v, 0, 3) == -1)
        goto errVec;

    if (dmVectorGet_int(v, 0, &idx0) == -1)
        goto errVec;

    printf("Value at index 0 after changing it to 3: %d\n", idx0);

    struct dmVector_float *cv = dmNewVectorWithCapacity_float(2);

    dmVectorPush_float(cv, 2.0);
    dmVectorPush_float(cv, 2.0);
    dmVectorPush_float(cv, 2.0);
    dmVectorPush_float(cv, 2.0);

    float result;

    if (dmVectorGet_float(cv, 3, &result) == -1) {
        printf("Error happened!\n");
        goto errCapVec;
    }

    printf("Value at index 4 for limited vector is: %f\n", result);

errCapVec:
    dmFreeVector_float(cv);

errVec:
    dmFreeVector_int(v);
}

int main(void)
{
    doStack();
    doVector();

    int res;

    DOUBLE(2.0, res);

    printf("Double result is %d\n", res);

    printf("Hello world!\n");

    printf("Min: between 2 and 3: %d\n", min_int(2, 3));
    printf("Min: between 1.5 and 1.0: %f\n", min_double(1.5, 1.0));

    return 0;
}
