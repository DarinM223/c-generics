#ifndef VECTOR_H
#define VECTOR_H

#include <stdlib.h>

/*
 * Contains a generic implementation of a resizable array.
 */

static const int defaultCapacity = 100;

#define MAKEVEC(TYPE)\
\
\
struct dmVector_##TYPE {\
    int size;\
    int capacity;\
    TYPE *array;\
};\
\
\
struct dmVector_##TYPE *dmNewVectorWithCapacity_##TYPE(int capacity)\
{\
    struct dmVector_##TYPE *v = (struct dmVector_##TYPE *) malloc(sizeof(struct dmVector_##TYPE));\
    v->capacity = capacity;\
    v->size = 0;\
    v->array = (TYPE*) malloc(sizeof(TYPE) * v->capacity);\
    return v;\
}\
\
\
struct dmVector_##TYPE *dmNewVector_##TYPE(void)\
{\
    return dmNewVectorWithCapacity_##TYPE(defaultCapacity);\
}\
\
\
void dmVectorPush_##TYPE(struct dmVector_##TYPE *v, TYPE data)\
{\
    if (v->size == v->capacity) {\
        v->capacity *= 2;\
        v->array = (TYPE*) realloc(v->array, sizeof(TYPE) * v->capacity);\
    }\
    v->array[v->size] = data;\
    v->size++;\
}\
\
\
int dmVectorSet_##TYPE(struct dmVector_##TYPE *v, int index, TYPE data)\
{\
    if (index >= v->size)\
        return -1;\
    v->array[index] = data;\
    return 0;\
}\
\
\
int dmVectorGet_##TYPE(struct dmVector_##TYPE *v, int index, TYPE *result)\
{\
    if (index >= v->size)\
        return -1;\
    *result = v->array[index];\
    return 0;\
}\
\
\
void dmFreeVector_##TYPE(struct dmVector_##TYPE *v)\
{\
    free(v->array);\
    free(v);\
}


MAKEVEC(int);
MAKEVEC(double);
MAKEVEC(float);

#endif
