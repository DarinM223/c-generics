#ifndef STACK_H
#define STACK_H

/* A singly linked list denoting a stack. */
struct dmStackNode {
    void *data;
    struct dmStackNode *next;
};

/* A stack structure containing the top node. */
struct dmStack {
    struct dmStackNode *top;
};

struct dmStack *dmNewStack(void);

/* Push a new value onto the stack. */
void dmStackPush(struct dmStack *stack, void *data);

/* Pop a value from the stack into result. Returns 0 if successful, -1 otherwise */
int dmStackPop(struct dmStack *stack, void **result);

void dmFreeStack(struct dmStack *stack);

#endif
